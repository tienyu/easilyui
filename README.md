# EasilyUI.h Project
The EasilyUI.h Project is used to help a beginner developer to make a program that have a GUI, and it based on NCurses.h(or curses.h).

## EasilyUI Library Installation
To use EasilyUI.h, you can download the `tar.gz` files and extract it into a directory like this:  
```bash
mkdir ~/.easilyui
cd ~/.easilyui
tar xf ../Downloads/EasilyUI.h-v*.tar.gz
```
And link it into the system include directory (e.g. `/usr/share/include` ) like this:  
```bash
ln -s ~/.easilyui/EasilyUI.h/easilyui.h /usr/share/include/easilyui.h
```
  
Then well done!
